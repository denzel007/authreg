<?php

namespace Tetrapak07\Authreg;

use Phalcon\Mvc\Controller;
use Supermodule\ControllerBase as SupermoduleBase;
use App\Models\EmailConfirmations;
use App\Forms\LoginForm;
use App\Forms\SignUpForm;
use App\Forms\ForgotPasswordForm;
use App\Auth\Exception as AuthException;
use App\Models\Users;
use App\Models\ResetPasswords;
use Tetrapak07\FlashMessage\FlashMessage;

class AuthregBase extends Controller
{ 
    
    protected function initialize()
    {        
         $this->view->setViewsDir($this->config->modules->authreg->viewsDir);
         $this->view->setTemplateBefore($this->config->modules->authreg->templateBefore);
         $this->view->projectName = $this->config->modules->authreg->projectName;
         $this->view->title = $this->config->modules->authreg->title;
    }

    public function onConstruct()
    {
        if (!SupermoduleBase::checkAndConnectModule('authreg')) {
            header("Location: /error/show404");
            exit;
        }   
    }
    
    public function sendEmail($user)
    {
       $emailConfirmation = new EmailConfirmations();

       $emailConfirmation->usersId = $user->id;
       $emailConfirmation->save();
 
    }
    
    public function indexAction()
    {
        return $this->response->redirect($this->config->modules->authreg->loginLink);
    }

    /**
     * Allow a user to signup to the system
     */
    public function signupAction()
    {
        
        $form = new SignUpForm();

        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost()) != false) {

                $user = new Users([
                    'name' => $this->request->getPost('name', 'striptags'),
                    'email' => $this->request->getPost('email', 'email'),
                    'password' => $this->security->hash($this->request->getPost('password', ['striptags', 'string'])),
                ]);
                

                if ($user->save()) {
                    
                    $this->sendEmail($user);
                    
                    return $this->dispatcher->forward([
                        'controller' => 'index',
                        'action' => 'index'
                    ]);
                }

                $this->flash->error(implode(';',$user->getMessages()));//$this->flash->error($user->getMessages());
            }
        }

        $this->view->form = $form;
        
    }

    /**
     * Starts a session in the admin backend
     */
    public function loginAction()
    {
        $form = new LoginForm();
        try {

            if (!$this->request->isPost()) {
                
                if ($this->auth->hasRememberMe()) {
                    return $this->auth->loginWithRememberMe();
                }
            } else {

                if ($form->isValid($this->request->getPost()) == false) {
                   
                    foreach ($form->getMessages() as $message) {
                       
                        $this->message('error', $message);
                    }
                } else {
              
                    $ret = $this->auth->check([
                        'email' => $this->request->getPost('email', 'email'),
                        'password' => $this->request->getPost('password', ['striptags', 'string']),
                        'remember' => $this->request->getPost('remember')
                    ]);

                    if ($ret) {
                      $role = isset( $this->auth->getIdentity()['groups'][0] ) ? $this->auth->getIdentity()['groups'][0] : 'guest';
                        $redirPath =  $this->config->modules->authreg->successLoginRedirectRoles[$role];
                       if (isset($redirPath) && ($redirPath!='')) {
                         return $this->response->redirect($redirPath);
                       } else {
                           return $this->response->redirect($this->config->modules->authreg->successLoginRedirect); 
                       }  
                     # return $this->response->redirect($this->config->modules->authreg->successLoginRedirect);
                    }
                     return $this->response->redirect($this->config->modules->authreg->loginLink);
                }
            }
        } catch (AuthException $e) {
          
             $this->message('error', $e->getMessage());
        }
        
        $this->view->form = $form;
        
        if ($this->session->has('auth-identity')) {
            $role = isset( $this->auth->getIdentity()['groups'][0] ) ? $this->auth->getIdentity()['groups'][0] : 'guest';
            $redirPath =  $this->config->modules->authreg->successLoginRedirectRoles[$role];
           if (isset($redirPath) && ($redirPath!='')) {
             return $this->response->redirect($redirPath);
           } else {
               return $this->response->redirect($this->config->modules->authreg->successLoginRedirect); 
           }
        }
    }

    /**
     * Shows the forgot password form
     */
    public function forgotPasswordAction()
    {
        $form = new ForgotPasswordForm();
        $ret = false;

        if ($this->request->isPost()) {
            
            # Send emails only is config value is set to true
            if ($this->getDI()->get('config')->modules->authreg->useMail) {

                if ($form->isValid($this->request->getPost()) == false) {
                    foreach ($form->getMessages() as $message) {
                       
                         $this->message('error', $message);
                    }
                } else {

                    $user = Users::findFirstByEmail($this->request->getPost('email', 'email'));
                    if (!$user) {
                       
                         $this->message('error', 'There is no account associated to this email');
                    } else {

                        $resetPassword = new ResetPasswords();
                        $resetPassword->usersId = $user->id;
                        if ($resetPassword->save()) {
                            
                             $this->message('success', 'Success! Please check your messages for an email reset password');
                             $ret = true;
                        } else {
                            foreach ($resetPassword->getMessages() as $message) {
                                
                                 $this->message('error', $message);
                            }
                        }
                    }
                }
            } else {
                
                 $this->message('warning', 'Emails are currently disabled. Change config key "useMail" to true to enable emails.');
            }
        }

        $this->view->form = $form;
        return $ret;
    }

    /**
     * Closes the session
     */
    public function logoutAction()
    {
        $this->auth->remove();
        return $this->response->redirect('index');
    }
    
    public function message($type = 'error', $mess = "Not found!", $dataUrl = '', $redirect = false)
    {
       if ((!$this->config->modules->authreg->showSuccess)AND($type == 'success')) {
           return false;
       }
       if ((!$this->config->modules->authreg->showError)AND($type == 'error')) {
           return false;
       }
       if ((!$this->config->modules->authreg->showWarning)AND($type == 'warning')) {
           return false;
       }
       $message = new FlashMessage();
       $message->initialize();
       return $message->message($type, $dataUrl, $mess, $redirect);
    }
}   