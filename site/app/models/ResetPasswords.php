<?php
namespace App\Models;

use Phalcon\Mvc\Model;
use App\Helpers\MailHelper;
use Tetrapak07\FlashMessage\FlashMessage;

/**
 * ResetPasswords
 * Stores the reset password codes and their evolution
 */
class ResetPasswords extends Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $usersId;

    /**
     *
     * @var string
     */
    public $code;

    /**
     *
     * @var integer
     */
    public $createdAt;

    /**
     *
     * @var integer
     */
    public $modifiedAt;

    /**
     *
     * @var string
     */
    public $reset;

    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate()
    {
        // Timestamp the confirmaton
        $this->createdAt = time();

        // Generate a random confirmation code
        $this->code = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(24)));

        // Set status to non-confirmed
        $this->reset = 'N';
    }

    /**
     * Sets the timestamp before update the confirmation
     */
    public function beforeValidationOnUpdate()
    {
        // Timestamp the confirmaton
        $this->modifiedAt = time();
    }

    /**
     * Send an e-mail to users allowing him/her to reset his/her password
     */
    public function afterCreate()
    {
       /* $this->getDI()
            ->getMail()
            ->send([
                $this->user->email => $this->user->name
            ], "Reset your password", 'reset', [
                'resetUrl' => '/reset-password/' . $this->code . '/' . $this->user->email
            ]);
        * 
        */
        $resetUrl = '/reset-password/' . $this->code . '/' . $this->user->email;
        $html = MailHelper::renderOut([], 'emailTemplates/reset', ['user' => $this->user, 'resetUrl' => $resetUrl]);

       if (!$html) {
           $this->message('error',  $mess  = 'Reset password E-mail not send. Maybe emailer module off');
       }
        MailHelper::sendMailOut(strtolower($this->user->email), "Reset your password", $html, $this->getDI()->getConfig()->modules->authreg->company);
        $this->message('success',  $mess  = 'Reset password E-mail was sent.');
    }

    public function initialize()
    {
        $this->belongsTo('usersId', __NAMESPACE__ . '\Users', 'id', [
            'alias' => 'user'
        ]);
    }
    
    public function message($type = 'error', $mess = "Not found!", $dataUrl = '', $redirect = false)
    {
       $message = new FlashMessage();
       $message->initialize();
       return $message->message($type, $dataUrl, $mess, $redirect);
    }
}
