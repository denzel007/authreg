<?php

defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('DS') || define('DS', DIRECTORY_SEPARATOR);
defined('APP_PATH') || define('APP_PATH', BASE_PATH . ''.DS.'app');
defined('VENDOR_COMPANY') || define('VENDOR_COMPANY','tetrapak07');
defined('MODULES_PATH') || define('MODULES_PATH', BASE_PATH.DS.'vendor'.DS.'tetrapak07');

return new \Phalcon\Config([
    
    'modules' => [
        
      # ADD THIS PART IN YOUR MAIN CONFIG and remove this file from you root config dir NOT FROM VENDOR DIR (!)  
      'authreg' => [
          'viewsDir'       => APP_PATH .DS.'views'.DS.'authreg',
          'loginLink' => 'authreg/login',
          'logoutLink' => 'authreg/logout', 
          'projectName' => 'Project',
          'title' => 'Auth and Reg',
          'successLoginRedirect' => 'dashboard',
          'publicUrl' => 'general.loc',
          'protocol' => 'http',
          'company' => 'Company',
          'useMail' => true
        ] 

    ],
    
]);

