<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <title>{% block title %} {% endblock %} - {{projectName}}</title>
        {{ stylesheet_link('modules/authreg/css/bootstrap.min.css') }}
        {{ stylesheet_link('modules/authreg/css/style.css') }}
        {% block css %} {% endblock %}
</head>

    <body class="skin-black">

                    {% block content %} 
                    {% endblock %}
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    {% block js %} 
    {% endblock %}

    {{ partial("_partials/footer") }}   

    </body>
    
</html>