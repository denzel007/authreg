<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class LocationsMigration_104
 */
class LocationsMigration_104 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('locations', [
                'columns' => [
                    new Column(
                        'id',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 10,
                            'first' => true
                        ]
                    ),
                    new Column(
                        'name',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "",
                            'size' => 255,
                            'after' => 'id'
                        ]
                    ),
                    new Column(
                        'isHeadoffice',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "0",
                            'notNull' => true,
                            'size' => 1,
                            'after' => 'name'
                        ]
                    ),
                    new Column(
                        'logo',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'isHeadoffice'
                        ]
                    ),
                    new Column(
                        'address',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "",
                            'size' => 255,
                            'after' => 'logo'
                        ]
                    ),
                    new Column(
                        'email',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "",
                            'size' => 255,
                            'after' => 'address'
                        ]
                    ),
                    new Column(
                        'fax',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "",
                            'size' => 255,
                            'after' => 'email'
                        ]
                    ),
                    new Column(
                        'phone',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "",
                            'size' => 255,
                            'after' => 'fax'
                        ]
                    ),
                    new Column(
                        'facebook',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'phone'
                        ]
                    ),
                    new Column(
                        'twitter',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'facebook'
                        ]
                    ),
                    new Column(
                        'instagram',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'twitter'
                        ]
                    ),
                    new Column(
                        'gplus',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'instagram'
                        ]
                    ),
                    new Column(
                        'web',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "",
                            'size' => 255,
                            'after' => 'gplus'
                        ]
                    ),
                    new Column(
                        'lat',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "0",
                            'notNull' => true,
                            'size' => 255,
                            'after' => 'web'
                        ]
                    ),
                    new Column(
                        'lon',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "0",
                            'notNull' => true,
                            'size' => 255,
                            'after' => 'lat'
                        ]
                    ),
                    new Column(
                        'companyId',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 10,
                            'after' => 'lon'
                        ]
                    ),
                    new Column(
                        'image1',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'companyId'
                        ]
                    ),
                    new Column(
                        'image2',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'image1'
                        ]
                    ),
                    new Column(
                        'image3',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'image2'
                        ]
                    ),
                    new Column(
                        'day1start',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'image3'
                        ]
                    ),
                    new Column(
                        'day2start',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day1start'
                        ]
                    ),
                    new Column(
                        'day3start',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day2start'
                        ]
                    ),
                    new Column(
                        'day4start',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day3start'
                        ]
                    ),
                    new Column(
                        'day5start',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day4start'
                        ]
                    ),
                    new Column(
                        'day6start',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day5start'
                        ]
                    ),
                    new Column(
                        'day7start',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day6start'
                        ]
                    ),
                    new Column(
                        'day1end',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day7start'
                        ]
                    ),
                    new Column(
                        'day2end',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day1end'
                        ]
                    ),
                    new Column(
                        'day3end',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day2end'
                        ]
                    ),
                    new Column(
                        'day4end',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day3end'
                        ]
                    ),
                    new Column(
                        'day5end',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day4end'
                        ]
                    ),
                    new Column(
                        'day6end',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day5end'
                        ]
                    ),
                    new Column(
                        'day7end',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day6end'
                        ]
                    ),
                    new Column(
                        'day1LunchStart',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day7end'
                        ]
                    ),
                    new Column(
                        'day2LunchStart',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day1LunchStart'
                        ]
                    ),
                    new Column(
                        'day3LunchStart',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day2LunchStart'
                        ]
                    ),
                    new Column(
                        'day4LunchStart',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day3LunchStart'
                        ]
                    ),
                    new Column(
                        'day5LunchStart',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day4LunchStart'
                        ]
                    ),
                    new Column(
                        'day6LunchStart',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day5LunchStart'
                        ]
                    ),
                    new Column(
                        'day7LunchStart',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day6LunchStart'
                        ]
                    ),
                    new Column(
                        'day1LunchEnd',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day7LunchStart'
                        ]
                    ),
                    new Column(
                        'day2LunchEnd',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day1LunchEnd'
                        ]
                    ),
                    new Column(
                        'day3LunchEnd',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day2LunchEnd'
                        ]
                    ),
                    new Column(
                        'day4LunchEnd',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day3LunchEnd'
                        ]
                    ),
                    new Column(
                        'day5LunchEnd',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day4LunchEnd'
                        ]
                    ),
                    new Column(
                        'day6LunchEnd',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day5LunchEnd'
                        ]
                    ),
                    new Column(
                        'day7LunchEnd',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "00:00",
                            'size' => 11,
                            'after' => 'day6LunchEnd'
                        ]
                    )
                ],
                'indexes' => [
                    new Index('PRIMARY', ['id'], 'PRIMARY'),
                    new Index('companyId', ['companyId'], null)
                ],
                'references' => [
                    new Reference(
                        'locations_ibfk_1',
                        [
                            'referencedTable' => 'companies',
                            'columns' => ['companyId'],
                            'referencedColumns' => ['id'],
                            'onUpdate' => 'CASCADE',
                            'onDelete' => 'CASCADE'
                        ]
                    )
                ],
                'options' => [
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '29',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ],
            ]
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

    /**
     * This method is called after the table was created
     *
     * @return void
     */
     public function afterCreateTable()
     {
        $this->batchInsert('locations', [
                'id',
                'name',
                'isHeadoffice',
                'logo',
                'address',
                'email',
                'fax',
                'phone',
                'facebook',
                'twitter',
                'instagram',
                'gplus',
                'web',
                'lat',
                'lon',
                'companyId',
                'image1',
                'image2',
                'image3',
                'day1start',
                'day2start',
                'day3start',
                'day4start',
                'day5start',
                'day6start',
                'day7start',
                'day1end',
                'day2end',
                'day3end',
                'day4end',
                'day5end',
                'day6end',
                'day7end',
                'day1LunchStart',
                'day2LunchStart',
                'day3LunchStart',
                'day4LunchStart',
                'day5LunchStart',
                'day6LunchStart',
                'day7LunchStart',
                'day1LunchEnd',
                'day2LunchEnd',
                'day3LunchEnd',
                'day4LunchEnd',
                'day5LunchEnd',
                'day6LunchEnd',
                'day7LunchEnd'
            ]
        );
     }
}
